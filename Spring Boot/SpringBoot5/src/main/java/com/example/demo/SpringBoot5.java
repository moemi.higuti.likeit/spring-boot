package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot5 {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot5.class, args);
	}

}
