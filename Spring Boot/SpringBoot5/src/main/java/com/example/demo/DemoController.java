package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.validation.BindingResult;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

@Controller
public class DemoController {

	@Autowired
	BulletinBoardRepository repos;
	
	@Autowired
    DivisionRepository d_repos;

	/* 一覧画面（初期画面）への遷移  */
	@GetMapping
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView();
		List<BulletinBoard> list1 = repos.findAll();
		mav.setViewName("list");
		mav.addObject("bbs", list1);
		
		//分類テーブルの読み込み
        List<Division> list = d_repos.findAll();
        mav.addObject("lists", list);
		return mav;
	}

	/* Transition to new page */
	@GetMapping("/add")
	ModelAndView add() {
		ModelAndView mav = new ModelAndView();
		BulletinBoard data = new BulletinBoard();
		mav.addObject("bbs", data);
		mav.setViewName("users/new");
		//分類テーブルの読み込み
        List<Division> list = d_repos.findAll();
        mav.addObject("lists", list);
		return mav;
	}
	

	/* 編集画面への遷移  */
	@GetMapping("/edit")
	ModelAndView edit(@RequestParam int id) {
		ModelAndView mav = new ModelAndView();
		BulletinBoard data = repos.findById(id);
		mav.addObject("bbs", data);
		mav.setViewName("users/new");
		
		//分類テーブルの読み込み
        List<Division> list = d_repos.findAll();
        mav.addObject("lists", list);
		return mav;
	}
	
	/* transition to "new"  */
	@PostMapping("/save")
	@Transactional(readOnly=false)
	public ModelAndView save(
			@ModelAttribute("bbs") BulletinBoard user) {
		repos.saveAndFlush(user);
		return new ModelAndView("redirect:/");
	}

	/* transition to show page*/
	@GetMapping("/show")
	ModelAndView show(@RequestParam int id) {
		ModelAndView mav = new ModelAndView();
		BulletinBoard data = repos.findById(id);
		mav.addObject("bbs", data);
		mav.setViewName("show");
		//分類テーブルの読み込み
        Division div = d_repos.findById(data.getDivision());
        mav.addObject("div", div);
		return mav;
	}
	
//	/* transition to showpage*/
//	@GetMapping("/show")
//	ModelAndView show(@RequestParam int id) {
//		ModelAndView mav = new ModelAndView();
//		User data = repos.findById(id);
//		mav.addObject("formModel", data);
//		mav.setViewName("users/new");
//		return mav;
//	}
	
	/* Function of delete  */
	@PostMapping("/delete")
	@Transactional(readOnly=false)
	public ModelAndView delete(@RequestParam int id) {
		repos.deleteById(id);
		return new ModelAndView("redirect:/");
	}
//    
//	@PostMapping("/create")
//    @Transactional(readOnly=false)
//    public ModelAndView create(
//            @ModelAttribute("bbs") BulletinBoard bbs {
//        bbs.setCreateDate(new Date());
//        repos.saveAndFlush(bbs);
//        return new ModelAndView("redirect:users/list");
//    }
	
	
    @PostMapping("/create")
	@Transactional(readOnly=false)
	public ModelAndView create(
			@ModelAttribute("bbs") @Validated BulletinBoard bbs,
			BindingResult result) {
    	
    	if (result.hasErrors()) {
            ModelAndView mav = new ModelAndView();
          //分類テーブルの読み込み
            List<Division> list = d_repos.findAll();
            mav.addObject("lists", list);
            mav.setViewName("users/new");
            return mav;
        }
    	Date date = new Date();
    	bbs.setCreateDate(date);
		repos.saveAndFlush(bbs);
		return new ModelAndView("redirect:/");
	}
    
    /* 初期データ作成 */
    @PostConstruct
    public void init() throws ParseException {
    	
//    	//掲示板初期データの登録
//        BulletinBoard bbs1 = new BulletinBoard();
//        String strDate = "2018-01-11";
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = dateFormat.parse(strDate);
//        bbs1.setCreateDate(date);
//        bbs1.setTitle("帰社日について");
//        bbs1.setCreateUser("松江　太郎");
//        bbs1.setContent("帰社日は以下の通りです。");
//        bbs1.setDivision(1);
//        repos.saveAndFlush(bbs1);
//
//        bbs1 = new BulletinBoard();
//        String strDate1 = "2018-04-10";
//        SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
//        Date date1 = dateFormat1.parse(strDate1);
//        bbs1.setCreateDate(date1);
//        bbs1.setTitle("新入社員歓迎会のお知らせ");
//        bbs1.setCreateUser("松江　太郎");
//        bbs1.setContent("新入社員歓迎会を実施します");
//        bbs1.setDivision(1);
//        repos.saveAndFlush(bbs1);
//    	
        //分類テーブル初期データの登録
        Division div1 = new Division();
        div1.setId(1);
        div1.setName("通達/連絡");
        d_repos.saveAndFlush(div1);

        div1 = new Division();
        div1.setId(2);
        div1.setName("会議開催について");
        d_repos.saveAndFlush(div1);

        div1 = new Division();
        div1.setId(3);
        div1.setName("スケジュール");
        d_repos.saveAndFlush(div1);

        div1 = new Division();
        div1.setId(4);
        div1.setName("イベント");
        d_repos.saveAndFlush(div1);

        div1 = new Division();
        div1.setId(5);
        div1.setName("その他");
        d_repos.saveAndFlush(div1);
    }
    
    
    
}