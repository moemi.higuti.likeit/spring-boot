package com.example.demo;

import com.example.demo.BulletinBoard;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BulletinBoardRepository extends JpaRepository<BulletinBoard, Long>{

	public BulletinBoard findById(int id);
	public void deleteById(int id);
   // User findByUsername(String username);
//	public BulletinBoard findAll(String username);
//	public Optional<BulletinBoard> findById(String username);
//	public void deleteById(String username);
	//public  void saveAndFlush(BulletinBoard bbs);
  
    
}