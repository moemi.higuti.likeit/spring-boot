package com.example.demo;


import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name="BulletinBoard")
public class BulletinBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int id;
    
    @Column
    (nullable = false)
    @NotEmpty
    private String title;
    
    @Column(nullable = false)
    @NotEmpty
    private String content;
    
    @Column
    private String createUser;
    @Column
    private Date createDate;
    @Column
    private int division;
	
}