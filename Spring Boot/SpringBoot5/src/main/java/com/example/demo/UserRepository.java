package com.example.demo;

import com.example.demo.BulletinBoard;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String>{

    public User findByUsername(String username);
    public Optional<User> findById(String username);
//	public BulletinBoard findAll(String username);
//	public void deleteById(String username);
//	public <S> S saveAndFlush(BulletinBoard bbs);

}