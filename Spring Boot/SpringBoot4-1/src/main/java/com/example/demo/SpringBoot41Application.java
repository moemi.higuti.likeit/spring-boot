package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot41Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot41Application.class, args);
	}

}
